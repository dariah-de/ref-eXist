(:~ 
 : 
 : This module serves for querying the GND (Gemeinsame Normdatei) by using the
 : SRU interface.
 : 
 : @author Wolfang Pempe
 : @author Ubbo Veentjer
 : :)
 
xquery version "3.1";
module namespace pndquery="http://textgrid.info/namespaces/xquery/pndquery";
declare default element namespace "http://textgrid.info/namespaces/vocabularies/pnd";
declare namespace http="http://expath.org/ns/http-client"; 
declare namespace srw="http://www.loc.gov/zing/srw/";
declare namespace gndo="https://d-nb.info/standards/elementset/gnd#";
declare namespace rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";

declare variable $pndquery:MAX_RECORDS as xs:integer := 50;
declare variable $pndquery:SRU_URL as xs:string := "http://services.dnb.de/sru/authorities";

declare function pndquery:build-request-url($ln as xs:string) {
    let $cql := "dnb.per%3D" || encode-for-uri($ln) || "*%20and%20BBG%3DTp*"
    let $url := $pndquery:SRU_URL || "?operation=searchRetrieve&amp;version=1.1&amp;query=" || $cql 
                || "&amp;recordSchema=RDFxml&amp;maximumRecords=" || $pndquery:MAX_RECORDS
                
    return $url
};

declare function pndquery:build-id-request-url($id as xs:string) {
    let $cql := "dnb.nid%3D" || $id || "%20and%20BBG%3DTp*"
    let $url := $pndquery:SRU_URL || "?operation=searchRetrieve&amp;version=1.1&amp;query=" || $cql 
                || "&amp;recordSchema=RDFxml&amp;maximumRecords=" || $pndquery:MAX_RECORDS
                
    return $url
};

declare %private function pndquery:normalize-date-node($date-node as node()*) {
    if ($date-node/@rdf:datatype eq "http://www.w3.org/2001/XMLSchema#date") then
        try {
                year-from-date(xs:date($date-node/text()))
        } catch * {
            "err: " || $date-node
        }
            else
                $date-node/text()
};

declare %private function pndquery:build-info($record as node()) as xs:string* {
        
        let $info := if ($record/gndo:biographicalOrHistoricalInformation) then
                string-join($record/gndo:biographicalOrHistoricalInformation/text(), ";") || "; "
            else ()
        let $birth := if($record//gndo:dateOfBirth) then
                "*" || pndquery:normalize-date-node($record//gndo:dateOfBirth[1]) || " "
            else ()
        let $birthplace := if($record//gndo:placeOfBirth) then
                "(*" || $record//gndo:placeOfBirth//gndo:preferredNameForThePlaceOrGeographicName/text() || ") "
            else if($record//gndo:placeOfBirthAsLiteral) then
                "(*" || $record//gndo:placeOfBirthAsLiteral/text() || ") "
            else ()
        let $death := if($record//gndo:dateOfDeath) then
                "†" || pndquery:normalize-date-node($record//gndo:dateOfDeath[1]) || " "
            else ()
        let $deathplace := if ($record//gndo:placeOfDeath) then
                "(†" || $record//gndo:placeOfDeath//gndo:preferredNameForThePlaceOrGeographicName/text() || ") "
            else ()

        let $active := if($record//gndo:periodOfActivity) then
                " active around " || string(($record//gndo:periodOfActivity)[1])
            else ()
        let $livingplace := if($record//gndo:placeOfActivity) then
            " living in " || string-join($record//gndo:placeOfActivity//gndo:preferredNameForThePlaceOrGeographicName/text(), ", ")
            else ()
        
        return $info || $birth || $birthplace || $death || $deathplace || $active || $livingplace
};

declare %private function pndquery:build-match($record as node(), $ln as xs:string) as xs:string* {
    if(starts-with($record/gndo:preferredNameForThePerson, $ln)) then
        $record/gndo:preferredNameForThePerson/text()
    else if(starts-with($record/gndo:variantNameForThePerson, $ln)) then
        ($record/gndo:variantNameForThePerson[starts-with(., $ln)]/text())[1]
    else
        $record/gndo:preferredNameForThePerson/text()
};

declare function pndquery:build-ln-response($ln as xs:string, $result as node()) {

    for $record in $result//rdf:RDF/rdf:Description
    
        let $id := $record/gndo:gndIdentifier/text()
        let $match := pndquery:build-match($record, $ln)
        let $prefname := $record/gndo:preferredNameForThePerson/text()
        
        let $info := pndquery:build-info($record)
        
        order by $match
        
        return 
            <person id="pnd:{$id}">
                <match_name>{string-join($match, "blubba")}</match_name>
                <preferred_name>{$prefname}</preferred_name>
                { for $variant in $record/gndo:variantNameForThePerson/text()
                    return <variant>{$variant}</variant>
                }
                <info>{$info}</info>
                <link target="{$record/@rdf:about}"/>
            </person>

};

declare %private function pndquery:build-old-data-elem($newnodes as node()*, $oldname) {

    for $newnode in $newnodes
        return
            element { $oldname } { $newnode/text() }  
        
};

declare function pndquery:build-id-response($id as xs:string, $result as node()) {
    
    let $record := $result//rdf:RDF/rdf:Description
    
    let $id := $record/gndo:gndIdentifier/text()

    return
        <entry id="{$id}">
            {pndquery:build-old-data-elem($record/gndo:biographicalOrHistoricalInformation, "biographicalInformation")}
            {pndquery:build-old-data-elem($record/gndo:dateOfBirth, "dateOfBirth")}
            {pndquery:build-old-data-elem($record/gndo:dateOfDeath, "dateOfDeath")}
            {pndquery:build-old-data-elem($record/gndo:placeOfBirth//gndo:preferredNameForThePlaceOrGeographicName, "placeOfBirth")}
            {pndquery:build-old-data-elem($record/gndo:placeOfDeath//gndo:preferredNameForThePlaceOrGeographicName, "placeOfDeath")}
            {pndquery:build-old-data-elem($record/gndo:placeOfActivity//gndo:preferredNameForThePlaceOrGeographicName, "placeOfResidence")}
            {pndquery:build-old-data-elem($record/gndo:preferredNameForThePerson, "preferredNameForThePerson")}
            {pndquery:build-old-data-elem($record/gndo:preferredNameForThePerson, "si")}
            {pndquery:build-old-data-elem($record/gndo:variantNameForThePerson, "variantNameForThePerson")}
            {pndquery:build-old-data-elem($record/gndo:variantNameForThePerson, "si")}
        </entry>
};

declare function pndquery:build-ac-response($ac as xs:string, $result as node()) {
    
    for $record in $result//rdf:RDF/rdf:Description
    
        let $id := $record/gndo:gndIdentifier/text()
        let $info := pndquery:build-info($record)
        let $match := pndquery:build-match($record, $ac)
        let $prefname := $record/gndo:preferredNameForThePerson/text()
        
        order by $match
        
        return 
            <person id="{$id}">
                <pname>
                    <name>{$match}</name>
                    {
                    if ($match eq $prefname) then
                        <ref>[{$id}]</ref>
                    else
                        <ref>&#x2192; {$prefname} [{$id}]</ref>
                        
                    }
                </pname>
                <info>{$info}</info>
            </person>
    
};

declare
    %rest:GET
    %rest:path("/pndsearch/pndquery.xql")
    %rest:query-param("ln", "{$ln}", "")
    %rest:query-param("ac", "{$ac}", "")
    %rest:query-param("id", "{$id}", "")
function pndquery:query($ln as xs:string*, $ac as xs:string*, $id as xs:string*) {
   let $response := 
   (: autocompletion :)
        if ($ac ne "") then
            let $url := pndquery:build-request-url($ac[1])
            
            let $req := <http:request href="{$url}" method="get">
                            <http:header name="Connection" value="close"/>
                       </http:request>
            let $result := http:send-request($req)[2]
            
            return pndquery:build-ac-response($ac[1], $result)
        
        (: one entry by id :)
	    else if ($id ne "") then
	        let $url := pndquery:build-id-request-url($id[1])
    
            let $req := <http:request href="{$url}" method="get">
                            <http:header name="Connection" value="close"/>
                       </http:request>
            let $result := http:send-request($req)[2]
            
            return pndquery:build-id-response($id[1], $result)
        
        (: long format :)    
	    else if ($ln ne "") then
            let $url := pndquery:build-request-url($ln[1])
            
            let $req := <http:request href="{$url}" method="get">
                            <http:header name="Connection" value="close"/>
                       </http:request>
            let $result := http:send-request($req)[2]
            
            return pndquery:build-ln-response($ln[1], $result)
        else
            "Error: Bad request (empty string?)" 
return
    <response n="{count($response//person)}">{ 
$response
    } </response>
    

};


(: json has utf8 issues, ommiting the media type works: https://github.com/eXist-db/exist/issues/353 :)
declare
    %rest:GET
    %rest:path("/pndsearch/json")
    %rest:query-param("ln", "{$ln}", "")
    %rest:query-param("ac", "{$ac}", "")
    %rest:query-param("id", "{$id}", "")
    %rest:produces("application/json")
(: %output:media-type("application/json") :)
    %output:method("json")      
function pndquery:jsonquery($ln as xs:string*, $ac as xs:string*, $id as xs:string*) {
    
    <response>{ 
        (: autocompletion :)
        if ($ac ne "") then
            let $url := pndquery:build-request-url($ac[1])
            
            let $req := <http:request href="{$url}" method="get">
                            <http:header name="Connection" value="close"/>
                       </http:request>
            let $result := http:send-request($req)[2]
            
            return pndquery:build-ac-response($ac[1], $result)
        
        (: one entry by id :)
	    else if ($id ne "") then
	        let $url := pndquery:build-id-request-url($id[1])
    
            let $req := <http:request href="{$url}" method="get">
                            <http:header name="Connection" value="close"/>
                       </http:request>
            let $result := http:send-request($req)[2]
            
            return pndquery:build-id-response($id[1], $result)
        
        (: long format :)    
	    else if ($ln ne "") then
            let $url := pndquery:build-request-url($ln[1])
            
            let $req := <http:request href="{$url}" method="get">
                            <http:header name="Connection" value="close"/>
                       </http:request>
            let $result := http:send-request($req)[2]
            
            return pndquery:build-ln-response($ln[1], $result)
        else
            "Error: Bad request (empty string?)"
    } </response>
    

};

declare
    %rest:GET
    %rest:path("/pndsearch/rdf")
    %rest:query-param("ln", "{$ln}", "")
    %rest:query-param("ac", "{$ac}", "")
    %rest:query-param("id", "{$id}", "")
function pndquery:rdfquery($ln as xs:string*, $ac as xs:string*, $id as xs:string*) {
    
        <rdf:RDF 
            xmlns:gndo="http://d-nb.info/standards/elementset/gnd#" 
            xmlns:owl="http://www.w3.org/2002/07/owl#" 
            xmlns:foaf="http://xmlns.com/foaf/0.1/">{ 
        (: autocompletion :)
        let $url := if ($ac ne "") then
            pndquery:build-request-url($ac[1])
        
        (: one entry by id :)
	    else if ($id ne "") then
	        pndquery:build-id-request-url($id[1])
        
        (: long format :)    
	    else if ($ln ne "") then
            pndquery:build-request-url($ln[1])
        else
            ()
            
        let $req := <http:request href="{$url}" method="get">
                        <http:header name="Connection" value="close"/>
                   </http:request>
        let $result := http:send-request($req)[2]
        
        return $result//rdf:RDF/rdf:Description

    } </rdf:RDF>
};

