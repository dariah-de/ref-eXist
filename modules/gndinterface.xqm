(:~
 : This module provides the new API for the DARIAH-DE authority service that
 : queries the GND provided by the German National Library (DNB). It is inspired by the former
 : API that uses a dump of the GND-data, but in difference to that it uses
 : the current SRU endpoint of DNB to retrieve data. It still contains references to the
 : PND (Personennormendatei), which was a predecessor of the GND and was aggregated
 : in the GND in 2012
 :
 : @author Wolfang Pempe
 : @author Michelle Weidling
 : @author Ubbbo Veentjer
 : @version 1.0
 :
 :  :)

xquery version "3.1";

module namespace gndxq="http://textgrid.info/namespaces/xquery/gndquery";

import module namespace pndquery="http://textgrid.info/namespaces/xquery/pndquery" at "pndquery.xql";

(: default element namespace still refers to pnd for backwards compatability :)
declare default element namespace "http://textgrid.info/namespaces/vocabularies/pnd";
declare namespace rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace http = "http://expath.org/ns/http-client";
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace test="http://exist-db.org/xquery/xqsuite";


(: the GND data license, c.f. https://spdx.org/licenses/CC0-1.0.html :)
(: https://www.dnb.de/EN/Service/DigitaleDienste/LinkedData/linkeddata_node.html :)
declare variable $gndxq:data-license := "CC0-1.0";

(:~
 : Takes a given query term as the beginning of the search string and performs
 : a wildcard search (e.g. ringeln* where the query term is ringeln).
 :
 : @author Ubbo Veentjer
 : @param $term a query term (e.g. ringeln)
 : @param $details true for a detailed output
 : @return a list of entries
 : :)
declare
    %rest:GET
    %rest:path("/gnd/v1/starts-with")
    %rest:query-param("q", "{$term}")
    %rest:query-param("details", "{$details}")
    %rest:produces("application/xml")
    %output:media-type("application/xml")
    %output:method("xml")
    %test:name("starts-with basic XML")
    %test:args("ringeln", "false")
    %test:assumeInternetAccess("http://services.dnb.de/sru/authorities")
    %test:assertXPath("$result//*:person[@id='118601121']")
function gndxq:autocomplete($term as xs:string*, $details as xs:boolean*) as element(response) {
    if($term ne "") then

        let $url := pndquery:build-request-url($term)
        let $result := gndxq:http-get($url)

        return if(not($details)) then
            let $response := pndquery:build-ac-response($term, $result)
            return <response data-license="{$gndxq:data-license}" n="{count($response//person)}">{$response}</response>

        else if ($details = true()) then
            let $response := pndquery:build-ln-response($term, $result)
            return <response data-license="{$gndxq:data-license}" n="{count($response//person)}">{$response}</response>

        else
            <response>Error: Unknown options. [GNDXQ0001]</response>
    else
        <response>Error: Bad request (empty query?). [GNDXQ0002]</response>
};

(:~
 : Takes a given query term as the beginning of the search string and performs
 : a wildcard search (e.g. ringeln* where the query term is ringeln).
 :
 : @author Ubbo Veentjer
 : @param $term a query term (e.g. ringeln)
 : @param $details true for a detailed output
 : @return a list of entries in JSON
 : :)
declare
    %rest:GET
    %rest:path("/gnd/v1/starts-with.json")
    %rest:query-param("q", "{$term}")
    %rest:query-param("details", "{$details}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
    %test:name("starts-with basic JSON")
    %test:args("ringeln", "false")
    %test:assumeInternetAccess("http://services.dnb.de/sru/authorities")
    %test:assertXPath("$result//*:person[@id='118601121']")
function gndxq:autocomplete-json($term as xs:string*, $details as xs:boolean*) as element(response) {
    gndxq:autocomplete($term, $details)
};

(:~
 : Takes a given query term and performs an exact search.
 :
 : @author Ubbo Veentjer
 : @param $term a query term (e.g. ringelnatz)
 : @param $details true for a detailed output
 : @return a list of entries
 : :)
declare
    %rest:GET
    %rest:path("/gnd/v1/exact-match")
    %rest:query-param("q", "{$term}")
    %rest:query-param("details", "{$details}")
    %rest:produces("application/xml")
    %output:media-type("application/xml")
    %output:method("xml")
    %test:name("exact-match basic XML")
    %test:args("ringelnatz", "false")
    %test:assumeInternetAccess("http://services.dnb.de/sru/authorities")
    %test:assertXPath("$result//*:person[@id='118601121']")
function gndxq:exact-match($term as xs:string*, $details as xs:boolean*) as element(response) {
    if($term ne "") then

        let $url := gndxq:build-exact-request-url($term)
        let $result := gndxq:http-get($url)

        return if(not($details)) then
            let $response := pndquery:build-ac-response($term, $result)
            return <response data-license="{$gndxq:data-license}" n="{count($response//person)}">{$response}</response>

        else if ($details = true()) then
            let $response := pndquery:build-ln-response($term, $result)
            return <response data-license="{$gndxq:data-license}" n="{count($response//person)}">{$response}</response>

        else
            <response>Error: Unknown options. [GNDXQ0003]</response>
    else
        <response>Error: Bad request (empty query?). [GNDXQ0004]</response>
};

(:~
 : Takes a given query term and performs an exact search.
 :
 : @author Ubbo Veentjer
 : @param $term a query term (e.g. ringelnatz)
 : @param $details true for a detailed output
 : @return a list of entries in JSON
 : :)
declare
    %rest:GET
    %rest:path("/gnd/v1/exact-match.json")
    %rest:query-param("q", "{$term}")
    %rest:query-param("details", "{$details}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
    %test:name("starts-with basic JSON")
    %test:args("ringelnatz", "false")
    %test:assumeInternetAccess("http://services.dnb.de/sru/authorities")
    %test:assertXPath("$result//*:person[@id='118601121']")
function gndxq:exact-match-json($term as xs:string*, $details as xs:boolean*) as element(response) {
    gndxq:exact-match($term, $details)
};


(:~
 : Return dataset for a specific id
 :
 : @author Ubbo Veentjer
 : @param $id the id (e.g. 143982044)
 : @return the dataset in xml
 : :)
declare
    %rest:GET
    %rest:path("/gnd/v1/{$id}")
    %test:arg("id", "143982044")
function gndxq:id($id as xs:string) {

    let $url := pndquery:build-id-request-url($id)
    let $result := gndxq:http-get($url)
    return pndquery:build-id-response($id, $result)

};

(:~
 : Return dataset for a specific id
 :
 : @author Ubbo Veentjer
 : @param $id the id (e.g. 143982044)
 : @return the dataset in rdf/xml
 : :)
declare
    %rest:GET
    %rest:path("/gnd/v1/{$id}.rdf")
    %test:arg("id", "143982044")
function gndxq:id-rdf($id as xs:string) {

    let $url := pndquery:build-id-request-url($id)
    let $result := gndxq:http-get($url)

    return
        <rdf:RDF
            xmlns:gndo="http://d-nb.info/standards/elementset/gnd#"
            xmlns:owl="http://www.w3.org/2002/07/owl#"
            xmlns:foaf="http://xmlns.com/foaf/0.1/">
            { $result//rdf:RDF/rdf:Description }
        </rdf:RDF>

};

(:~
 : Return dataset for a specific id
 :
 : @author Ubbo Veentjer
 : @param $id the id (e.g. 143982044)
 : @return the dataset in json
 : :)
declare
    %rest:GET
    %rest:path("/gnd/v1/{$id}.json")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
    %test:arg("id", "143982044")
function gndxq:id-json($id as xs:string) {

    let $url := pndquery:build-id-request-url($id)
    let $result := gndxq:http-get($url)
    return pndquery:build-id-response($id, $result)

};



declare %private function gndxq:build-exact-request-url($ln as xs:string) {
    let $cql := "dnb.per%3D" || encode-for-uri($ln) || "%20and%20BBG%3DTp*"
    let $url := $pndquery:SRU_URL || "?operation=searchRetrieve&amp;version=1.1&amp;query=" || $cql
                || "&amp;recordSchema=RDFxml&amp;maximumRecords=" || $pndquery:MAX_RECORDS

    return $url
};

declare %private function gndxq:http-get($url as xs:string) {
        let $req := <http:request href="{$url}" method="get">
                        <http:header name="Connection" value="close"/>
                   </http:request>
        return http:send-request($req)[2]
};
