xquery version "3.1";
module namespace tgxq="http://textgrid.info/namespaces/xquery/tgnquery";
declare default element namespace "http://textgrid.info/namespaces/vocabularies/tgn";
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";

declare     
    %rest:GET
    %rest:path("/tgnsearch/tgnquery.xql")
    %rest:query-param("ac", "{$ac}")
    %rest:query-param("id", "{$id}")
    %rest:query-param("ln", "{$ln}")
    %rest:query-param("geo", "{$geo}")
function tgxq:query($ac as xs:string*, $id as xs:string*, $ln as xs:string*, $geo as xs:boolean*) {
    
    		if ($ac ne "") then
              <response>{tgxq:get-terms-ac($ac, $geo)}</response>
    		else if ($id ne "") then
    		  <response>{tgxq:get-data-set($id)}</response>
    		else if ($ln ne "") then
    		  <response>{tgxq:get-terms-ln($ln)}</response>
            else
              <response>Error: Bad request (empty string?)</response>
    		
};

declare     
    %rest:GET
    %rest:path("/tgnsearch/json")
    %rest:query-param("ac", "{$ac}")
    %rest:query-param("id", "{$id}")
    %rest:query-param("ln", "{$ln}")
    %rest:query-param("geo", "{$geo}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")    
function tgxq:jquery($ac as xs:string*, $id as xs:string*, $ln as xs:string*, $geo as xs:boolean*) {
    
    		if ($ac ne "") then
              <response>{tgxq:get-terms-ac($ac, $geo)}</response>
    		else if ($id ne "") then
    		  <response>{tgxq:get-data-set($id)}</response>
    		else if ($ln ne "") then
    		  <response>{tgxq:get-terms-ln($ln)}</response>
        else
          <response>Error: Bad request (empty string?)</response>
    		
};

(: compatibility interface for datasheet editor, see https://gitlab.gwdg.de/dariah-de/ref-eXist/issues/4 - get terms by exact match, not starts-with :)
declare
    %rest:GET
    %rest:path("/tgnsearch/tgnquery2.xql")
    %rest:query-param("ac", "{$ac}")
    %rest:query-param("id", "{$id}")
    %rest:query-param("ln", "{$ln}")
function tgxq:query-em($ac as xs:string*, $id as xs:string*, $ln as xs:string*) {

  		if ($ac ne "") then
            <response>{tgxq:get-terms-em($ac)}</response>
  		else if ($id ne "") then
  		  <response>{tgxq:get-data-set($id)}</response>
  		else if ($ln ne "") then
  		  <response>{tgxq:get-terms-ln($ln)}</response>
      else
        <response>Error: Bad request (empty string?)</response>

};

declare %private function tgxq:get-terms-ac($qterm as xs:string, $geo as xs:boolean* ) {
    for $a in collection("/db/tgn")//Term_Text[ngram:starts-with(., $qterm)]
        let $sid := concat("tgn:", $a/ancestor::Subject/@Subject_ID)
        let $path := $a/ancestor::Subject/Hierarchy/text()
		let $i := $path[ends-with(., "Europe | World")]
		order by $a, $i empty greatest
        return <term id="{$sid}">
                <name>{$a/text()}</name>
                <path>{$path}</path>
                { 
                    if($geo) then (
                        <latitude>{$a/ancestor::Subject//Latitude/Decimal/text()}</latitude>,
                        <longitude>{$a/ancestor::Subject//Longitude/Decimal/text()}</longitude>
                    )
                    else ()
                }                
               </term>
};

declare %private function tgxq:get-terms-ln($qterm as xs:string) {
    for $a in collection("/db/tgn")//Term_Text[ngram:starts-with(., $qterm)]
        let $lid := $a/ancestor::Subject/@Subject_ID
        let $sid := concat("tgn:", $lid)
        let $pref:= $a/ancestor::Subject//Preferred_Term/Term_Text/text()
        let $path := $a/ancestor::Subject/Hierarchy/text()
		let $i := $path[ends-with(., "Europe | World")]
		order by $a, $i empty greatest
        return <term id="{$sid}"><match_name>{$a/text()}</match_name><preferred_name>{$pref}</preferred_name>
               {for $v in $a/ancestor::Subject//Term_Text
                  return <variant>{$v/text()}</variant>}
               <path>{$path}</path>
               <link target="http://textgridlab.org/tgnsearch/tgnquery.xql?id={$lid}"/></term>
};

declare %private function tgxq:get-data-set($id as xs:string) {

    let $tgnid := if(starts-with($id, "tgn:"))
        then substring($id, 5)
        else $id

	let $a := collection("/db/tgn")//Subject[ngram:starts-with(@Subject_ID, $tgnid)][1]
	return $a
};

(: compatibility function for datasheet editor, see https://gitlab.gwdg.de/dariah-de/ref-eXist/issues/4 - get terms by exact match, not starts-with :)
declare function tgxq:get-terms-em($qterm as xs:string) {
    let $search := fn:concat('^', $qterm, '$')
    for $a in collection("/db/tgn")//Term_Text[ngram:wildcard-contains(., $search)]
        let $sid := concat("tgn:", $a/ancestor::Subject/@Subject_ID)
        let $path := $a/ancestor::Subject/Hierarchy/text()
        let $lat := $a/ancestor::Subject//Latitude/Decimal/text()
        let $long := $a/ancestor::Subject//Longitude/Decimal/text()
    let $i := $path[ends-with(., "Europe | World")]
    order by $a, $i empty greatest

        return <term id="{$sid}">
                 <name>{$a/text()}</name>
                 <path>{$path}</path>
                 <latitude>{$lat}</latitude>
                 <longitude>{$long}</longitude>
               </term>
};

