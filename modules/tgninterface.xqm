(:~
 : This module provides the new API for the DARIAH-DE authority service that
 : queries the Getty Thesaurus of Geographic Names. It is inspired by the former
 : API that uses a dump of the Getty data, but in difference to that it uses
 : the current SPARQL endpoint of Getty to retrieve data.
 :
 : @author Wolfang Pempe
 : @author Michelle Weidling
 : @author Ubbbo Veentjer
 : @version 1.0
 :
 :  :)

xquery version "3.1";

module namespace tgnxq="http://textgrid.info/namespaces/xquery/tgnquery-new";

import module namespace tgnxq-b="http://textgrid.info/namespaces/xquery/tgnquery-backwarts-compatible" at "tgn-backwarts-compatible.xqm";

declare default element namespace "http://textgrid.info/namespaces/vocabularies/tgn";
declare namespace http = "http://expath.org/ns/http-client";
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace schema="http://schema.org/";
declare namespace sparql= "http://www.w3.org/2005/sparql-results#";
declare namespace test="http://exist-db.org/xquery/xqsuite";

(: the TGN data license, c.f. https://spdx.org/licenses/ODC-By-1.0.html :)
declare variable $tgnxq:data-license := "ODC-By-1.0";

(:~
 : Takes a given query term as the beginning of the search string and performs
 : a wildcard search (e.g. berl* where the query term is berl).
 :
 : @author Michelle Weidling
 : @param $term a query term (e.g. vagen)
 : @param $georef true if geo coordinates should be included in the output
 : @param $details true for a detailed output
 : @return a list of entries
 : :)
declare
    %rest:GET
    %rest:path("/tgn/v1/starts-with")
    %rest:query-param("q", "{$term}")
    %rest:query-param("georef", "{$georef}")
    %rest:query-param("details", "{$details}")
    %rest:produces("application/xml")
    %output:media-type("application/xml")
    %output:method("xml")

    %test:arg("term", "vagen")
function tgnxq:autocomplete($term as xs:string*, $georef as xs:boolean*,
$details as xs:boolean*) as element(response) {
    if($term ne "") then
        if(not($georef or $details)) then
            <response data-license="{$tgnxq:data-license}">{tgnxq:get-compact-result($term, false(), "auto")}</response>

        else if ($georef = true()) then
            <response data-license="{$tgnxq:data-license}">{tgnxq:get-compact-result($term, true(), "auto")}</response>

        else if ($details = true()) then
            <response data-license="{$tgnxq:data-license}">{tgnxq:get-detailed-result($term, "auto")}</response>

        else
            <response>Error: Unknown options.</response>
    else
        <response>Error: Bad request (empty query?).</response>
};


(:~
 : Takes a given query term as the beginning of the search string and performs
 : a wildcard search (e.g. berl* where the query term is berl).
 :
 : @author Michelle Weidling
 : @param $term a query term (e.g. berl)
 : @param $georef true if geo coordinates should be included in the output
 : @param $details true for a detailed output
 : @return a list of entries
 : :)
declare
    %rest:GET
    %rest:path("/tgn/v1/starts-with.json")
    %rest:query-param("q", "{$term}")
    %rest:query-param("georef", "{$georef}")
    %rest:query-param("details", "{$details}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function tgnxq:autocomplete-json($term as xs:string*, $georef as xs:boolean*,
$details as xs:boolean*) as element(response) {
    tgnxq:autocomplete($term, $georef, $details)
};


(:~
 : Takes a given query term and performs an exact search.
 :
 : @author Michelle Weidling
 : @param $term a query term (e.g. berlin)
 : @param $georef true if geo coordinates should be included in the output
 : @param $details true for a detailed output
 : @return a list of entries
 : :)
declare
    %rest:GET
    %rest:path("/tgn/v1/exact-match")
    %rest:query-param("q", "{$term}")
    %rest:query-param("georef", "{$georef}")
    %rest:query-param("details", "{$details}")
    %rest:produces("application/xml")
    %output:media-type("application/xml")
    %output:method("xml")
function tgnxq:exact-match($term as xs:string*, $georef as xs:boolean*,
$details as xs:boolean*) as element(response) {
    if($term ne "") then
        if(not($georef or $details)) then
            <response data-license="{$tgnxq:data-license}">{tgnxq:get-compact-result($term, false(), "exact")}</response>

        else if ($georef = true()) then
            <response data-license="{$tgnxq:data-license}">{tgnxq:get-compact-result($term, true(), "exact")}</response>

        else if ($details = true()) then
            <response data-license="{$tgnxq:data-license}">{tgnxq:get-detailed-result($term, "exact")}</response>

        else
            <response>Error: Unknown options.</response>
    else
        <response>Error: Bad request (empty query?).</response>
};


(:~
 : Takes a given query term and performs an exact search.
 :
 : @author Michelle Weidling
 : @param $term a query term (e.g. berlin)
 : @param $georef true if geo coordinates should be included in the output
 : @param $details true for a detailed output
 : @return a list of entries
 : :)
declare
    %rest:GET
    %rest:path("/tgn/v1/exact-match.json")
    %rest:query-param("q", "{$term}")
    %rest:query-param("georef", "{$georef}")
    %rest:query-param("details", "{$details}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function tgnxq:exact-match-json($term as xs:string*, $georef as xs:boolean*,
$details as xs:boolean*) as element(response) {
    tgnxq:exact-match($term, $georef, $details)
};


(:~
 : The main interface. Handles requests.
 :
 : @author Ubbo Veentjer
 : @author Michelle Weidling
 : @param $input the API call after /tgn/
 : @param $term a query term (e.g. berl)
 : @param $georef true if geo coordinates should be included in the output
 : @param $details true for a detailed output
 : @return the full RDF data set
 : @error Invalid input
 :  :)
declare
    %rest:GET
    %rest:path("/tgn/v1/{$input}")
    function tgnxq:id($input as xs:string) {
    let $file-extension := if(contains($input, ".")) then substring-after($input, ".") else $input
    let $content-type :=
        switch ($file-extension)
            case "json" return "application/json"
            case "jsonld" return "application/json"
            case "n3" return "text/plain"
            case "nt" return "text/plain"
            default return "application/xml"

    return
    try {
        (<rest:response>
            <http:response status="200">
            <http:header name="Content-Language" value="en"/>
            <http:header name="Content-Type" value="{$content-type}"/>
            </http:response>
        </rest:response>,
        tgnxq:result-by-id($input))
    } catch * {
        error(QName("http://textgrid.info/namespaces/xquery/tgnquery-new", "TGNXQ001"), "Invalid input " || $input || ".")
    }
};


(:~
 : Returns the full RDF data set of a given Getty ID.
 :
 : @author Michelle Weidling
 : @param $id the given TGN ID of a place
 : @return the full RDF data set
 :  :)
declare function tgnxq:result-by-id($input as xs:string) {
    let $id := local:clear-id($input)
    let $file-extension := local:get-file-extension($input)
    return
        switch ($file-extension)
            case "rdf" return
                doc("http://vocab.getty.edu/tgn/" || $id || "." || $file-extension)

            case "json" return
                local:get-response($file-extension, $id)

            case "jsonld" return
                local:get-response($file-extension, $id)

            case "n3" return
                local:get-response($file-extension, $id)

            case "nt" return
                local:get-response($file-extension, $id)

            default return
                error(QName("http://textgrid.info/namespaces/xquery/tgnquery-new", "TGNXQ004"),
                "Invalid file extension " || $file-extension || ".")
};

declare function local:get-response($file-extension as xs:string, $id as xs:string) {

    let $uri :=
        xs:anyURI(escape-uri("http://vocab.getty.edu/download/" ||
            $file-extension ||
            "?uri=http://vocab.getty.edu/tgn/" ||
            $id ||
            "." ||
            $file-extension, false()))

    let $request :=
        <hc:request method="GET" href="{$uri}">
            <hc:header name="Accept" value="text/xml,application/json,text/plain;q=0.9,*/*;q=0.8"/>
            <hc:header name="Connection" value="close"/>
        </hc:request>
    return
        if($file-extension eq "n3") then
            hc:send-request($request)[2]
        else
            hc:send-request($request)[2]
            => util:base64-decode()
};

(:~
 : Returns the Getty ID without file extension.
 :
 : @author Michelle Weidling
 : @param $id the given TGN ID of a place
 : @return the ID as numeral (e.g. 1040686)
 :  :)
declare function local:clear-id($id as xs:string) as xs:integer {
    let $id :=
        if(contains($id, ".")) then
            substring-before($id, ".")
        else
            $id
    return
        if(matches($id, "[a-zA-Z]")) then
            error(QName("http://textgrid.info/namespaces/xquery/tgnquery-new", "TGNXQ002"), "Invalid ID " || $id || ".")
        else
            $id
};


(:~
 : Returns the file extension provided. Defaults to RDF.
 :
 : @author Michelle Weidling
 : @param $id the given TGN ID of a place
 : @return the file extension, e.g. ".json"
 :  :)
declare function local:get-file-extension($input as xs:string) as xs:string {
    let $file-extension :=
        if(contains($input, ".")) then
            substring-after($input, ".")
        else
            "rdf"
    return
        if(matches($file-extension, "[0-9]")
        and not($file-extension = "n3")) then
            error(QName("http://textgrid.info/namespaces/xquery/tgnquery-new", "TGNXQ003"), "Invalid file extension " || $input || ".")
        else
            $file-extension
};

(:~
 : Gets the search result for a non-detailed output.
 :
 : @author Michelle Weidling
 : @param $term a query term (e.g. berlin)
 : @param $georef true if geo coordinates should be included in the output
 : @param $auto-or-exact "auto" if a wildcard search should be performed
 : @return a list of places
 : :)
declare %private function tgnxq:get-compact-result($term as xs:string,
$georef as xs:boolean, $auto-or-exact as xs:string) as element(term)* {
    let $query-result := tgnxq:get-sparql($term, $auto-or-exact)
    let $results := $query-result//sparql:result
    let $no-of-hits := count($results)

    return
        for $iii in 1 to $no-of-hits
            let $id := substring-after($results[$iii]//sparql:binding[@name="GettyID"]//sparql:uri, "tgn/")
            let $sid := "tgn:" || $id
            let $name := local:get-info($results[$iii], "Term")
            let $name-variant := local:get-info($results[$iii], "label")
            let $path := local:get-info($results[$iii], "Parents")
            (: commas have to replaced to get same result as in old interface :)
            let $path := replace($path, ", ", " | ")
            let $longitude := local:get-info($results[$iii], "Longitude")
            let $latitude := local:get-info($results[$iii], "Latitude")

            return
                if(tgnxq-b:has-several-hits($id, $results[$iii])
                and tgnxq-b:is-first-with-id($id, $results[$iii])) then
                    (tgnxq:get-compact-summary($id, $results[$iii], $name, $path, $georef))
                else if(tgnxq-b:has-several-hits($id, $results[$iii])) then
                    ()
                else
                    <term id="{$sid}">
                        <name>{$name}</name>
                        {if($name ne $name-variant) then
                                <variant>{$name-variant}</variant>
                        else ()}
                        <path>{$path}</path>
                        {if($georef) then
                            (<longitude>{$longitude}</longitude>,
                            <latitude>{$latitude}</latitude>)
                        else()}
                    </term>
};



(:~
 : Gets the search result for a detailed output.
 :
 : @author Michelle Weidling
 : @param $term a query term (e.g. berlin)
 : @param $georef true if geo coordinates should be included in the output
 : @param $auto-or-exact "auto" if a wildcard search should be performed
 : @return a list of places
 : :)
declare %private function tgnxq:get-detailed-result($term as xs:string,
$auto-or-exact as xs:string) as element(term)* {
    let $query-result := tgnxq:get-sparql($term, $auto-or-exact)
    let $results := $query-result//sparql:result
    let $no-of-hits := count($results)

    return
        for $iii in 1 to $no-of-hits
            let $id := substring-after($results[$iii]//sparql:binding[@name="GettyID"]//sparql:uri, "tgn/")
            let $name := local:get-info($results[$iii], "Term")
            let $preferred-name := local:get-info($results[$iii], "preflabel")
            let $name-variant := local:get-info($results[$iii], "label")
            let $path := local:get-info($results[$iii], "Parents")
            (: commas have to replaced to get same result as in old interface :)
            let $path := replace($path, ", ", " | ")
            let $link := "http://textgridlab.org/tgnsearch/tgnquery.xql?id=" || $id
            let $rdf-link := "http://vocab.getty.edu/tgn/" || $id || ".rdf"
            let $longitude := local:get-info($results[$iii], "Longitude")
            let $latitude := local:get-info($results[$iii], "Latitude")

            return
                if(tgnxq-b:has-several-hits($id, $results[$iii])
                and tgnxq-b:is-first-with-id($id, $results[$iii])) then
                    (tgnxq-b:summarize-entries($id, $results[$iii], $name, $preferred-name, $path))
                else if(tgnxq-b:has-several-hits($id, $results[$iii])) then
                    ()
                else
                    <term id="{concat("tgn:", $id)}">
                        <match_name>{$name}</match_name>
                        <preferred_name>
                            {if($preferred-name) then
                                $preferred-name
                            else $name}
                        </preferred_name>
                        {if($name ne $name-variant) then
                            <variant>{$name-variant}</variant>
                        else ()}
                        <path>{$path}</path>
                        <longitude>{$longitude}</longitude>
                        <latitude>{$latitude}</latitude>
                        <link target="{$link}"/>
                        <rdf-link target="{$rdf-link}"/>
                    </term>
};



declare function local:get-info($result as node()*, $key as xs:string) as text()? {
    $result/sparql:binding[@name = $key]/sparql:literal/text()
};

(:~
 : Creates a SPARQL query containg all queries neccessary and performs it.
 :
 : @author Michelle Weidling
 : @param $term a query term (e.g. berlin)
 : @param $auto-or-exact "auto" if a wildcard search should be performed
 : @return the SPARQL query result
 : :)
declare function tgnxq:get-sparql($term as xs:string, $auto-or-exact as xs:string) {
    let $query-head :=
        "prefix wgs84: <http://www.w3.org/2003/01/geo/wgs84_pos#>
         prefix foaf: <http://xmlns.com/foaf/0.1/>

        select * {
            ?GettyID luc:term """

    let $query-tail := """ ;
            gvp:prefLabelGVP [xl:literalForm ?Term];
            rdfs:label ?label ;
            skos:prefLabel ?preflabel .

            optional {?GettyID gvp:parentString ?Parents}

            optional {  ?GettyID foaf:focus ?placeref .
                        ?placeref wgs84:lat ?Latitude .
                        ?placeref wgs84:long ?Longitude . }

            optional {?GettyID skos:scopeNote [dct:language gvp_lang:en; rdf:value ?ScopeNote]}
            ?GettyID skos:inScheme tgn: .
            FILTER STRSTARTS(lcase(str(?label)), lcase(""" || $term || """)) .}
            ORDER BY ASC(?label)"

    let $query :=   $query-head ||
                    $term ||
                    (if($auto-or-exact = "auto") then "*" else ()) ||
                    $query-tail

    let $urlEncodedQuery as xs:string := encode-for-uri($query)
    let $reqUrl as xs:anyURI :=
        string-join(("http://vocab.getty.edu/sparql.xml?query=", $urlEncodedQuery, "&amp;implicit=true"))
        => xs:anyURI()
    let $request :=
        <hc:request method="get" href="{$reqUrl}">
            <hc:header name="Accept" value="text/xmlapplication/xml;q=0.9,*/*;q=0.8"/>
            <hc:header name="Connection" value="close"/>
        </hc:request>
    return
      hc:send-request($request)[2]
};


(:~
 : In case the SPARQL result has more than one hit that refers to a place this
 : function summarizes all hits that refer to the same TGN in one XML fragment.
 :
 : This is necessary since the SPARQL endpoint of the TGN doesn't accept SPARQL'S
 : GROUP BY statement which leads to several result entries when a place has
 : more than one rdfs:label. Thus, these entries have to be summarized
 : manually via XQuery.
 :
 : @author Michelle Weidling
 : @author Ubbo Veentjer
 : @param $id the given TGN ID of a place
 : @param $current-result the current sparql:reselt fragment
 : @param $name the place's name as given in gvp:prefLabelGVP
 : @param $path the location of a place as given in gvp:parentString
 : @param $georef true() if geo coordinates should be included
 : @return the summarized entry as XML
 : :)
declare function tgnxq:get-compact-summary($id as xs:string, $current-result as node(),
$name as xs:string, $path as xs:string, $georef as xs:boolean) as element(term) {
    let $same-places := $current-result/following::*[matches(.//sparql:uri, $id)]
    let $all-place-names := ($same-places/sparql:binding[@name="label"]/sparql:literal/string(), $name)
    let $variants := distinct-values($all-place-names)
    let $longitude := local:get-info($current-result, "Longitude")
    let $latitude := local:get-info($current-result, "Latitude")

    let $link := "http://textgridlab.org/tgnsearch/tgnquery.xql?id=" || $id
    let $rdf-link := "http://vocab.getty.edu/tgn/" || $id || ".rdf"

    return
        <term id="{concat("tgn:", $id)}">
            <match_name>{$name}</match_name>
            {if(count($variants) gt 1) then
                for $variant in $variants
                return
                    <variant>{$variant}</variant>
            else
                ()}
            <path>{$path}</path>
            {if($georef) then
                (<longitude>{$longitude}</longitude>,
                <latitude>{$latitude}</latitude>)
            else()}
        </term>
};
