(:~
 : This module provides the old API for the DARIAH-DE authority service that
 : queries the Getty Thesaurus of Geographic Names.
 :
 : @author Wolfang Pempe
 : @author Michelle Weidling
 : @author Ubbbo Veentjer
 : @version 1.0
 :
 :  :)

xquery version "3.1";

module namespace tgnxq-b="http://textgrid.info/namespaces/xquery/tgnquery-backwarts-compatible";

declare default element namespace "http://textgrid.info/namespaces/vocabularies/tgn";
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace schema="http://schema.org/";
declare namespace sparql= "http://www.w3.org/2005/sparql-results#";

(:~
 : TGN query - main entry point
 :
 : @deprecated
 : :)
declare
    %rest:GET
    %rest:path("/tgn/xml")
    %rest:query-param("ac", "{$ac}")
    %rest:query-param("id", "{$id}")
    %rest:query-param("ln", "{$ln}")
    %rest:query-param("geo", "{$geo}")
function tgnxq-b:query($ac as xs:string*, $id as xs:string*, $ln as xs:string*,
$geo as xs:boolean*) as element(response) {

    		if ($ac ne "") then
              <response>{tgnxq-b:get-terms-ac($ac, $geo)}</response>
    		else if ($id ne "") then
    		  <response>{tgnxq-b:get-data-set($id)}</response>
    		else if ($ln ne "") then
    		  <response>{tgnxq-b:get-terms-ln($ln)}</response>
            else
              <response>Error: Bad request (empty string?)</response>

};

(:~
 : TGN query
 :
 : @deprecated
 : :)
declare
    %rest:GET
    %rest:path("/tgn/json")
    %rest:query-param("ac", "{$ac}")
    %rest:query-param("id", "{$id}")
    %rest:query-param("ln", "{$ln}")
    %rest:query-param("geo", "{$geo}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function tgnxq-b:jquery($ac as xs:string*, $id as xs:string*, $ln as xs:string*,
$geo as xs:boolean*) as element(response) {

    		if ($ac ne "") then
              <response>{tgnxq-b:get-terms-ac($ac, $geo)}</response>
    		else if ($id ne "") then
    		  <response>{tgnxq-b:get-data-set($id)}</response>
(:    		else if ($ln ne "") then:)
(:    		  <response>{tgnxq-b:get-terms-ln($ln)}</response>:)
            else
              <response>Error: Bad request (empty string?)</response>

};


(:~
 : This function returns all entries in the TGN that start with a given term.
 : E.g. when searching for "Berl", the place "Berlin" is retrieved as well as the
 : "Barlow" which has the historical name "Berlei".
 :
 : In extension to the former tgnquery.xql of the DARIAH-DE authority service
 : this module return the contemporary place name as name-element, while
 : historical name variants are displayed in a variant-element. For each
 : historical name variant a separate result is retrieved. Identity can be
 : ascertained by the TGN ID.
 :
 : @author Wolfang Pempe
 : @author Michelle Weidling
 : @author Ubbo Veentjer
 : @param  the (fragment of a) place name
 : @return the query result as a custom XML
 :
 : :)
declare %private function tgnxq-b:get-terms-ac($term as xs:string,
$geo as xs:boolean*) as node()* {
    let $query-result := tgnxq-b:sparql-ac($term)
    let $results := $query-result//sparql:result
    let $no-of-hits := count($results)

    return
        for $iii in 1 to $no-of-hits
            let $sid := "tgn:" || substring-after($results[$iii]//sparql:uri, "tgn/")
            let $name := $results[$iii]/sparql:binding[@name="Term"]/sparql:literal/text()
            let $name-variant := $results[$iii]/sparql:binding[@name="label"]/sparql:literal/text()
            let $path := $results[$iii]/sparql:binding[@name="Parents"]/sparql:literal/text()
            (: commas have to replaced to get same result as in old interface :)
            let $path := replace($path, ", ", " | ")
            return
                <term id="{$sid}">
                    <name>{$name}</name>
                    {if($name ne $name-variant) then <variant>{$name-variant}</variant> else ()}
                    <path>{$path}</path>
                </term>
};


(:~
 : Sends a SPARQL query to TGN for the autocomplemetion parameter, i.e. it
 : returns all places whose rdfs:label starts with the given term.
 :
 : We decided to filter the label instead of the gvp:prefLabelGV because
 : otherwise historical names (that are stated in rdfs:label among others)
 : wouldn't appear in the query results.
 :
 : @author Wolfang Pempe
 : @author Michelle Weidling
 : @author Ubbo Veentjer
 : @param $term the (fragment of a) place name
 : @return the query result as XML
 : :)
declare function tgnxq-b:sparql-ac($term as xs:string) as node() {
    let $query-start :=
        "select * {
            ?GettyID luc:term """

    let $query-middle := """ ;
            gvp:prefLabelGVP [xl:literalForm ?Term];
            rdfs:label ?label .

            optional {?GettyID gvp:parentString ?Parents}

            optional {?GettyID skos:scopeNote [dct:language gvp_lang:en; rdf:value ?ScopeNote]}
            ?GettyID skos:inScheme tgn: ."
    let $query-end := "}"

    let $ac-filter := "FILTER STRSTARTS(lcase(str(?label)), lcase(""" || $term || """)) ."

    let $ac-order-by := "ORDER BY ASC(?label)"

    let $query := $query-start || $term || "*" || $query-middle || $ac-filter
        || $query-end || $ac-order-by

    let $urlEncodedQuery as xs:string := encode-for-uri($query)
    let $reqUrl as xs:anyURI := string-join(("http://vocab.getty.edu/sparql.xml?query=", $urlEncodedQuery, "&amp;implicit=true")) => xs:anyURI()
    
    let $request :=
        <hc:request method="GET" href="{$reqUrl}">
            <hc:header name="Accept" value="text/xmlapplication/xml;q=0.9,*/*;q=0.8"/>
            <hc:header name="Connection" value="close"/>
        </hc:request>
    return
        hc:send-request($request)[2]
    
};


(:~
 : This function returns all entries in the TGN that have a label equal to a
 : given term.
 :
 : The output is compliant to the old DARIAH-DE authority data service.
 :
 : @author Wolfang Pempe
 : @author Michelle Weidling
 : @author Ubbo Veentjer
 : @param  $term a place name
 : @return the query result as a custom XML
 :
 : :)
declare %private function tgnxq-b:get-terms-ln($term as xs:string) {
    let $query-result := tgnxq-b:sparql-ln($term)
    let $results := $query-result//sparql:result
    let $no-of-hits := count($results)

    return
        for $iii in 1 to $no-of-hits
            let $id := substring-after($results[$iii]//sparql:uri, "tgn/")
            let $name := $results[$iii]/sparql:binding[@name="Term"]/sparql:literal/text()
            let $preferred-name := $results[$iii]/sparql:binding[@name="preflabel"]/sparql:literal/text()
            let $name-variant := $results[$iii]/sparql:binding[@name="label"]/sparql:literal/text()
            let $path := $results[$iii]/sparql:binding[@name="Parents"]/sparql:literal/text()
            (: commas have to replaced to get same result as in old interface :)
            let $path := replace($path, ", ", " | ")
            let $link := "http://textgridlab.org/tgnsearch/tgnquery.xql?id=" || $id
            let $rdf-link := "http://vocab.getty.edu/tgn/" || $id || ".rdf"

            return
                if(tgnxq-b:has-several-hits($id, $results[$iii])
                and tgnxq-b:is-first-with-id($id, $results[$iii])) then
                    (tgnxq-b:summarize-entries($id, $results[$iii], $name, $preferred-name, $path))
                else if(tgnxq-b:has-several-hits($id, $results[$iii])) then
                    ()
                else
                    <term id="{concat("tgn:", $id)}">
                        <match_name>{$name}</match_name>
                        <preferred_name>{if($preferred-name) then $preferred-name else $name}</preferred_name>
                        {if($name ne $name-variant) then <variant>{$name-variant}</variant> else ()}
                        <path>{$path}</path>
                        <link target="{$link}"/>
                        <rdf-link target="{$rdf-link}"/>
                    </term>
};


(:~
 : Sends a SPARQL query to TGN for the autocomplemetion parameter, i.e. it
 : returns all places whose rdfs:label starts with the given term.
 :
 : We decided to filter the label instead of the gvp:prefLabelGV because
 : otherwise historical names (that are stated in rdfs:label among others)
 : wouldn't appear in the query results.
 : TODO
 : @author Wolfang Pempe
 : @author Michelle Weidling
 : @author Ubbo Veentjer
 : @param $term the (fragment of a) place name
 : @return the query result as XML
 : :)
declare function tgnxq-b:sparql-ln($term as xs:string) as node() {
    let $query-start :=
        "select * {
            ?GettyID luc:text """

    let $query-middle := """ ;
            gvp:prefLabelGVP [xl:literalForm ?Term];
            rdfs:label ?label ;
            skos:prefLabel ?preflabel .

            optional {?GettyID gvp:parentString ?Parents}

            optional {?GettyID skos:scopeNote [dct:language gvp_lang:en; rdf:value ?ScopeNote]}
            ?GettyID skos:inScheme tgn: ."
    let $query-end := "}"

    let $filter := "FILTER STRSTARTS(lcase(str(?label)), lcase(""" || $term || """)) ."
    (:TODO: ordering should be improved:)
    let $order-by := "ORDER BY ASC(?term)"

    let $query := $query-start || $term || $query-middle || $filter
        || $query-end || $order-by

    let $urlEncodedQuery as xs:string := encode-for-uri($query)
    let $reqUrl as xs:anyURI := string-join(("http://vocab.getty.edu/sparql.xml?query=", $urlEncodedQuery, "&amp;implicit=true")) => xs:anyURI()

    let $request :=
        <hc:request method="GET" href="{$reqUrl}">
            <hc:header name="Accept" value="text/xmlapplication/xml;q=0.9,*/*;q=0.8"/>
            <hc:header name="Connection" value="close"/>
        </hc:request>
    return
        hc:send-request($request)[2]
};

(:~
 : Retrieves an RDF file from Getty with all information about a given TGN ID.
 :
 : @author Wolfang Pempe
 : @author Michelle Weidling
 : @author Ubbo Veentjer
 : @param $id the given TGN ID of a place
 : @return the query result as RDF
 : :)
declare %private function tgnxq-b:get-data-set($id as xs:string) {
    let $reqUrl as xs:anyURI := string-join(("http://vocab.getty.edu/tgn/", $id, ".rdf")) => xs:anyURI()
    
    let $request :=
        <hc:request method="GET" href="{$reqUrl}">
            <hc:header name="Accept" value="text/xmlapplication/xml;q=0.9,*/*;q=0.8"/>
            <hc:header name="Connection" value="close"/>
        </hc:request>
    return
        hc:send-request($request)[2]
};

(:~
 : Checks if there are several sparql:result XML fragments that refer to the
 : same place.
 :
 : @author Michelle Weidling
 : @param $id the given TGN ID of a place
 : @param $current-result the current sparql:reselt fragment
 : @return xs:boolean
 : :)
declare function tgnxq-b:has-several-hits($id as xs:string, $current-result as node())
as xs:boolean {
    if($current-result/following::*[matches(.//sparql:uri, $id)]
        or $current-result/preceding::*[matches(.//sparql:uri, $id)]) then
        true()
    else
        false()
};


(:~
 : Checks if a given sparql:result XML fragment is the first of several hits that
 : refer to the same place.
 :
 : @author Michelle Weidling
 : @param $id the given TGN ID of a place
 : @param $current-result the current sparql:reselt fragment
 : @return xs:boolean
 : :)
declare function tgnxq-b:is-first-with-id($id as xs:string, $current-result as node())
as xs:boolean {
    let $prev-same-places := $current-result/preceding::*[matches(.//sparql:uri, $id)]
    let $next-same-places := $current-result/following::*[matches(.//sparql:uri, $id)]
    return
        if($next-same-places and not($prev-same-places)) then
            true()
        else
            false()
};

(:~
 : In case the SPARQL results has more than one hit that refers to a place this
 : function summarizes all hits that refer to the same TGN in one XML fragment.
 :
 : This is necessary since the SPARQL endpoint of the TGN doesn't accept SPARQL'S
 : GROUP BY statement which leads to several result entries when a place has
 : more than one rdfs:label. Thus, these entries have to be summarized
 : manually via XQuery.
 :
 : TODO: adapt link element
 :
 : @author Michelle Weidling
 : @param $id the given TGN ID of a place
 : @param $current-result the current sparql:reselt fragment
 : @param $name the place's name as given in gvp:prefLabelGVP
 : @param $preferred-name the place's name as given in skos:prefLabel
 : @param $path the location of a place as given in gvp:parentString
 : @return the summarized entry as XML
 : :)
declare function tgnxq-b:summarize-entries($id as xs:string, $current-result as node(),
$name as xs:string, $preferred-name as xs:string, $path as xs:string) as node() {
    let $same-places := $current-result/following::*[matches(.//sparql:uri, $id)]
    let $all-place-names := ($same-places/sparql:binding[@name="label"]/sparql:literal/string(), $name)
    let $variants := distinct-values($all-place-names)

    let $link := "http://textgridlab.org/tgnsearch/tgnquery.xql?id=" || $id
    let $rdf-link := "http://vocab.getty.edu/tgn/" || $id || ".rdf"

    return
        <term id="{concat("tgn:", $id)}">
            <match_name>{$name}</match_name>
            <preferred_name>{if($preferred-name) then $preferred-name else $name}</preferred_name>
            {if(count($variants) gt 1) then
                for $variant in $variants
                return
                    <variant>{$variant}</variant>
            else
                ()}
            <path>{$path}</path>
            <link target="{$link}"/>
            <rdf-link target="{$rdf-link}"/>
        </term>
};
