xquery version "3.1";

(: This is the main test script for the new API at modules/tgninterface.xqm.
 : It is supposed to run during post installation stage.
 :
 : @version 1.0.0
 : @author Michelle Weidling
 : :)

module namespace tgn-test = "http://textgrid.info/namespaces/xquery/tgn-test";

import module namespace tgnxq="http://textgrid.info/namespaces/xquery/tgnquery-new" at "modules/tgninterface.xqm";

declare namespace tgn="http://textgrid.info/namespaces/vocabularies/tgn";
declare namespace test="http://exist-db.org/xquery/xqsuite";


(: Test to test the test :)
declare
    %test:name("Assert FAIL")
    %test:args("<milestone/>")
    %test:assertFalse
    function tgn-test:test-basic-000($node as element(*)) {
        false()
};

(:######################### Starts-with ######################################:)

declare
    %test:name("Starts-with basic XML")
    %test:args("pöhlde", "false", "false")
    %test:assumeInternetAccess("http://vocab.getty.edu/")
    %test:assertXPath("$result//*[@id=""tgn:1040686""]")
    function tgn-test:starts-with-001($term as xs:string*, $georef as xs:boolean*, $details as xs:boolean*) {
        tgnxq:autocomplete($term, $georef, $details)
};

declare
    %test:name("Starts-with basic XML with coordinates")
    %test:args("pöhlde", "true", "false")
    %test:assumeInternetAccess("http://vocab.getty.edu/")
    %test:assertXPath("$result//*[@id=""tgn:1040686""]/*[local-name() = ""latitude""]")
    function tgn-test:starts-with-002($term as xs:string*, $georef as xs:boolean*, $details as xs:boolean*) {
        tgnxq:autocomplete($term, $georef, $details)
};


declare
    %test:name("Starts-with basic XML with details")
    %test:args("pöhlde", "false", "true")
    %test:assumeInternetAccess("http://vocab.getty.edu/")
    %test:assertXPath("$result//*[@id=""tgn:1040686""]/*[local-name() = ""rdf-link""]")
    function tgn-test:starts-with-003($term as xs:string*, $georef as xs:boolean*, $details as xs:boolean*) {
        tgnxq:autocomplete($term, $georef, $details)
};


declare
    %test:name("Starts-with basic JSON")
    %test:args("pöhlde", "false", "false")
    %test:assumeInternetAccess("http://vocab.getty.edu/")
    %test:assertTrue
    function tgn-test:starts-with-004($term as xs:string*, $georef as xs:boolean*, $details as xs:boolean*) {
        matches(serialize(tgnxq:autocomplete($term, $georef, $details)), "tgn:1040686")
};

declare
    %test:name("Starts-with basic JSON with geo coordinates")
    %test:args("pöhlde", "true", "false")
    %test:assumeInternetAccess("http://vocab.getty.edu/")
    %test:assertTrue
    function tgn-test:starts-with-005($term as xs:string*, $georef as xs:boolean*, $details as xs:boolean*) {
        matches(serialize(tgnxq:autocomplete($term, $georef, $details)), "10\.316667")
};


declare
    %test:name("Starts-with basic JSON with full details")
    %test:args("pöhlde", "false", "true")
    %test:assumeInternetAccess("http://vocab.getty.edu/")
    %test:assertTrue
    function tgn-test:starts-with-006($term as xs:string*, $georef as xs:boolean*, $details as xs:boolean*) {
        matches(serialize(tgnxq:autocomplete($term, $georef, $details)), "http://vocab\.getty\.edu/tgn/1040686\.rdf")
};


(:############################ Exact match ###################################:)

declare
    %test:name("Exact-match basic XML")
    %test:args("pöhlde", "false", "false")
    %test:assumeInternetAccess("http://vocab.getty.edu/")
    %test:assertXPath("$result//*[@id=""tgn:1040686""]")
    function tgn-test:exact-match-001($term as xs:string*, $georef as xs:boolean*, $details as xs:boolean*) {
        tgnxq:exact-match($term, $georef, $details)
};

declare
    %test:name("Exact-match basic XML with coordinates")
    %test:args("pöhlde", "true", "false")
    %test:assumeInternetAccess("http://vocab.getty.edu/")
    %test:assertXPath("$result//*[@id=""tgn:1040686""]/*[local-name() = ""longitude""]")
    function tgn-test:exact-match-002($term as xs:string*, $georef as xs:boolean*, $details as xs:boolean*) {
        tgnxq:exact-match($term, $georef, $details)
};

declare
    %test:name("Exact-matchh basic XML with details")
    %test:args("pöhlde", "false", "true")
    %test:assumeInternetAccess("http://vocab.getty.edu/")
    %test:assertXPath("$result//*[@id=""tgn:1040686""]/*[local-name() = ""rdf-link""]")
    function tgn-test:exact-match-003($term as xs:string*, $georef as xs:boolean*, $details as xs:boolean*) {
        tgnxq:exact-match($term, $georef, $details)
};


declare
    %test:name("Exact-match basic JSON")
    %test:args("pöhlde", "false", "false")
    %test:assumeInternetAccess("http://vocab.getty.edu/")
    %test:assertTrue
    function tgn-test:exact-match-004($term as xs:string*, $georef as xs:boolean*, $details as xs:boolean*) {
        matches(serialize(tgnxq:exact-match($term, $georef, $details)), "tgn:1040686")
};

declare
    %test:name("Exact-match basic JSON with geo coordinates")
    %test:args("pöhlde", "true", "false")
    %test:assumeInternetAccess("http://vocab.getty.edu/")
    %test:assertTrue
    function tgn-test:exact-match-005($term as xs:string*, $georef as xs:boolean*, $details as xs:boolean*) {
        matches(serialize(tgnxq:exact-match($term, $georef, $details)), "51\.616667")
};


declare
    %test:name("Exact-match basic JSON with full details")
    %test:args("pöhlde", "false", "true")
    %test:assumeInternetAccess("http://vocab.getty.edu/")
    %test:assertTrue
    function tgn-test:exact-match-006($term as xs:string*, $georef as xs:boolean*, $details as xs:boolean*) {
        matches(serialize(tgnxq:exact-match($term, $georef, $details)), "http://vocab\.getty\.edu/tgn/1040686\.rdf")
};


(:############################ ID ############################################:)

declare
    %test:name("ID match default")
    %test:args("7008959")
    %test:assumeInternetAccess("http://vocab.getty.edu/")
    %test:assertXPath("$result//*/string() =""Island of Skye""")
    function tgn-test:id-001($id as xs:string*) {
        tgnxq:id($id)
};

declare
    %test:name("ID match RDF")
    %test:args("7008959.rdf")
    %test:assumeInternetAccess("http://vocab.getty.edu/")
    %test:assertXPath("$result//*/string() =""Island of Skye""")
    function tgn-test:id-002($id as xs:string*) {
        tgnxq:id($id)
};

declare
    %test:name("ID match JSON")
    %test:args("7008959.json")
    %test:assumeInternetAccess("http://vocab.getty.edu/")
    %test:assertTrue
    function tgn-test:id-003($id as xs:string*) {
        matches(serialize(tgnxq:id($id)), "http://vocab.getty.edu/tgn/7008959")
};

declare
    %test:name("ID match JSONLD")
    %test:args("7008959.jsonld")
    %test:assumeInternetAccess("http://vocab.getty.edu/")
    %test:assertTrue
    function tgn-test:id-004($id as xs:string*) {
        matches(serialize(tgnxq:id($id)), "Island of Skye")
};

declare
    %test:name("ID match TTL")
    %test:args("7008959.n3")
    %test:assumeInternetAccess("http://vocab.getty.edu/")
    %test:assertTrue
    function tgn-test:id-005($id as xs:string*) {
        matches(serialize(tgnxq:id($id)), "Island of Skye")
};

declare
    %test:name("ID match N-Triples")
    %test:args("7008959.nt")
    %test:assumeInternetAccess("http://vocab.getty.edu/")
    %test:assertTrue
    function tgn-test:id-006($id as xs:string*) {
        matches(serialize(tgnxq:id($id)), "Island of Skye")
};
