xquery version "3.1";

import module namespace tgn-test = "http://textgrid.info/namespaces/xquery/tgn-test" at "test.xq";
import module namespace gnd-test = "http://textgrid.info/namespaces/xquery/gnd-test" at "test-gnd.xq";
import module namespace gndxq="http://textgrid.info/namespaces/xquery/gndquery" at "modules/gndinterface.xqm";

import module namespace test="http://exist-db.org/xquery/xqsuite" at "resource:org/exist/xquery/lib/xqsuite/xqsuite.xql";
import module namespace tests="https://sade.textgrid.de/ns/tests" at "tests.xqm";

(: the target collection into which the app is deployed :)
declare variable $target external;

let $sysout := util:log-system-out("TGN API installation done. running tests…")
let $tests := test:suite((
    util:list-functions("http://textgrid.info/namespaces/xquery/tgn-test"),
    util:list-functions("http://textgrid.info/namespaces/xquery/gnd-test"),
    util:list-functions("http://textgrid.info/namespaces/xquery/gndquery")
    ))

let $fileSeparator := util:system-property("file.separator")
let $system-path := system:get-exist-home() || $fileSeparator

let $filename := $system-path || "tests-tgn-results.xml"
let $file := file:serialize($tests, $filename, ())
let $print := util:log-system-out( tests:test-function-rate($tests, $target) )

return
    util:log-system-out("tests complete. See " || $filename)

