xquery version "3.1";

module namespace gnd-test = "http://textgrid.info/namespaces/xquery/gnd-test";
import module namespace gndxq="http://textgrid.info/namespaces/xquery/gndquery" at "modules/gndinterface.xqm";

declare namespace http = "http://expath.org/ns/http-client";
declare namespace pnd="http://textgrid.info/namespaces/vocabularies/pnd";
declare namespace test="http://exist-db.org/xquery/xqsuite";

(: Test to test the test :)
declare
    %test:name("FAIL")
    %test:args("<milestone/>")
    %test:assertFalse
function gnd-test:test-basic-000($node as element(*)) {
        false()
};

declare
    %test:name("starts-with basic XML")
    %test:args("muschelkal", "false")
    %test:assumeInternetAccess("http://services.dnb.de/sru/authorities")
    %test:assertEquals(2)
function gnd-test:test-starts-with-000($term as xs:string*, $details as xs:boolean*) {
      gndxq:autocomplete($term, $details)//pnd:person => count()
};

declare
    %test:name("starts-with basic XML")
    %test:args("muschelkalk", "false")
    %test:assumeInternetAccess("http://services.dnb.de/sru/authorities")
    %test:assertEquals(2)
function gnd-test:test-starts-with-001($term as xs:string*, $details as xs:boolean*) {
      gndxq:autocomplete($term, $details)//pnd:person => count()
};

declare
    %test:name("starts-with basic XML with details")
    %test:args("ringeln", "true")
    %test:assumeInternetAccess("http://services.dnb.de/sru/authorities")
    %test:assertXPath("$result gt 10")
function gnd-test:test-starts-with-003($term as xs:string*, $details as xs:boolean*) {
      gndxq:autocomplete($term, $details)//pnd:person//pnd:variant => count()
};

declare
    %test:name("exact-match basic XML")
    %test:args("muschelkalk", "false")
    %test:assumeInternetAccess("http://services.dnb.de/sru/authorities")
    %test:assertEquals(2)
function gnd-test:test-exact-match-000($term as xs:string*, $details as xs:boolean*) {
      gndxq:exact-match($term, $details)//pnd:person => count()
};

declare
    %test:name("exact-match basic XML")
    %test:args("muschelkal", "false")
    %test:assumeInternetAccess("http://services.dnb.de/sru/authorities")
    %test:assertEquals(0)
function gnd-test:test-exact-match-001($term as xs:string*, $details as xs:boolean*) {
      gndxq:exact-match($term, $details)//pnd:person => count()
};

declare
    %test:name("exact-match basic XML with details")
    %test:args("ringelnatz", "true")
    %test:assumeInternetAccess("http://services.dnb.de/sru/authorities")
    %test:assertXPath("$result gt 10")
function gnd-test:test-exact-match-003($term as xs:string*, $details as xs:boolean*) {
      gndxq:exact-match($term, $details)//pnd:person//pnd:variant => count()
};
