FROM docker.io/existdb/existdb:6.3.0
ENV PROCESSOR="6.3.0"

# for testing the local build, copy it over and comment out the corresponding ADD statement
# COPY ./build/*.xar /exist/autodeploy/
ADD https://ci.de.dariah.eu/exist-repo/find?abbrev=ref-eXist-develop&processor=${PROCESSOR} /exist/autodeploy/ref.xar

ADD https://ci.de.dariah.eu/exist-repo/find?abbrev=openapi-develop&processor=${PROCESSOR} /exist/autodeploy/openapi.xar
